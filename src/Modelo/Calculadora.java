package Modelo;

import Util.Pila;

/**
 *
 * @author 1151759-1151788
 */
public class Calculadora {
    private Notacion notacion;
    private String resultado;

    /**

     * Constructor de Calculadora

     * @param notacion Cadena con la notacion en infija
     
     */
    
    public Calculadora(String notacion) throws Exception {
        Notacion n = new Notacion(notacion);
        this.notacion = n;
    }

    /**
    *  Metodo que retorna el resultado de la expresion
    * 
    * @return resultado como cadena
    */
    public String getResultado() throws Exception{
        String pos=this.getPostfija();
        System.out.println(pos);
        Pila<Integer> numeros = new Pila<>();
        String numTemp="";
        boolean esNegativo=false;
        for (int i = 0; i < pos.length(); i++) {
            if(pos.charAt(i)=='-'){
                if(i+1<pos.length()){
                    if(pos.charAt(i+1)!=' '){
                        esNegativo=true;
                    }
                }
            }
            if(pos.charAt(i)=='+'||(pos.charAt(i)=='-'&&!esNegativo)||pos.charAt(i)=='*'||pos.charAt(i)=='/'||pos.charAt(i)=='^'){
                int operando2=numeros.desapilar();
                int operando1=numeros.desapilar();
                int result=0;
                switch (pos.charAt(i)) {
                    case '+':
                        result=operando1+operando2;
                        break;
                    case '-':
                        result=operando1-operando2;
                        break;
                    case '*':
                        result=operando1*operando2;
                        break;
                    case '/':
                        if(operando2==0){
                            throw new Exception("Error Matematico");
                        }else{
                        result=operando1/operando2;
                        }
                        break;
                    case '^':
                        result=(int)Math.pow(operando1, operando2);
                        break;
                    default:
                        break;
                }
                numeros.apilar(result);
            }else if(pos.charAt(i)!=' '){
                  numTemp+=pos.charAt(i);   
            }else{
                if(!"".equals(numTemp)){
                  int numero=Integer.parseInt(numTemp);
                 numeros.apilar(numero);
                }
                numTemp="";
                esNegativo=false;
            }
        }
        
        return numeros.desapilar().toString();
    }
    /**
    *  Metodo que retorna la expresion en infija
    * 
    * @return expresion en infija
    */
    public String getInfija() {
        return notacion.getInfija();
    }
    /**
    *  Metodo que retorna la expresion en prefija
    * 
    * @return expresion en prefija
    */
    public String getPrefija() {
        return notacion.getPrefija();
    }
    /**
    *  Metodo que retorna la expresion en porfija
    * 
    * @return expresion en postfija
    */
    public String getPostfija() {
        return notacion.getPostfija();
    }
}