package Modelo;

import Util.Pila;

/**
 *
 * @author 1151759-1151788
 */
public class Notacion {
    /**
     * Expresion notacion infija
     */
    private String infija;

    /**

     * Constructor de Notacion

     * @param infija Cadena con la notacion en infija
     
     * Verifija si la expresion es correcta, si no lanza una excepcion. 

     */
    public Notacion(String infija) throws Exception {
        if (this.validarExpresion(infija)) {
            this.infija = infija;
        } else {
            throw new Exception("Error de sintaxis en la expresion");
        }
    }

     /**
    *  Metodo que me valida la expresion que se esta ingresando
    * @param exp cadena sobre el cual voy a realizar la validacion 
    * 
    * @return true si es correcta false si no lo es
    */
    public boolean validarExpresion(String exp) {
        Pila<Character> parentesis = new Pila<>();
        boolean flag = true; // bandera para validar que no hayan signos seguidos
        boolean flag2 = false; //bandera para validar que no hayan dos ~ seguidos
        for (int i = 0; i < exp.length(); i++) {
            if (!(exp.charAt(i) >= '0' && exp.charAt(i) <= '9')) {
                if (exp.charAt(i) == '(') {
                    parentesis.apilar('(');
                    if (i > 0) {
                        if (!(esSigno(exp.charAt(i - 1)) || exp.charAt(i - 1) == '(')) {
                            return false;
                        }
                    }
                } else if (exp.charAt(i) == ')') {
                    if (parentesis.esVacia()) {
                        return false;
                    } else if (!esSigno(exp.charAt(i - 1))) {
                        parentesis.desapilar();
                    }
                } else if (esSigno(exp.charAt(i)) && !flag ) {
                    flag = true;
                } else if(exp.charAt(i)=='~'){
                       flag2=true;
                }else{
                    return false;
                }
            } else {
                flag = false;
                flag2=false;
            }
        }
        return parentesis.esVacia() && !flag;
    }

     /**
    *  Metodo que me verifica si caracter ingresado es un signo
    * @param c caracter sobre el cual voy a realizar la validacion
    * @return c si el caracter ingresado es alguno de los signos
    */
    private boolean esSigno(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '^';
    }

    public String getInfija() {
        return infija;
    }

    public void setInfija(String infija) {
        this.infija = infija;
    }

     /**
    *  Metodo que me obtiene la expresion Prefija de la notacion 
    * @return expresion convertida en Prefija
    */
    public String getPrefija(){
        return this.getPrefija1();
    }
     /**
    *  Metodo que me obtiene la expresion Postfija de la notacion 
    * @return expresion convertida en Prefija
    */
    public String getPostfija(){
        return this.getPostfija1();
    }
    
    /**
    *  Metodo que me obtiene la expresion Prefija de la notacion 
    * @return expresion convertida en Prefija
    */
    private String getPrefija1() {
        String prefija = "";
        Pila<Character> aux = new Pila<>();
        char notacion[] = this.infija.toCharArray();
        for (int i = notacion.length - 1; i >= 0; i--) {
            if (notacion[i] == ')') {
                aux.apilar(notacion[i]);
            } else {
                if (esSigno(notacion[i])) {
                    if (aux.esVacia()) {
                        aux.apilar(notacion[i]);
                        prefija += " ";
                    } else {
                        while (esMayor(notacion[i], aux)) {
                            prefija += aux.desapilar();
                        }
                        prefija += " ";
                        aux.apilar(notacion[i]);
                    }
                } else if (notacion[i] == '(') {
                    char c = aux.desapilar();
                    while (c != ')') {
                        prefija += " " + c;
                        c = aux.desapilar();
                    }
                } else {
                    if(notacion[i]=='~'){
                        prefija+="-";
                    }else{
                        prefija += notacion[i];  
                    }
                }
            }
        }
        while (!aux.esVacia()) {
            prefija += " " + aux.desapilar();
        }
        String prefij="";
        for(int i=prefija.length()-1;i>=0; i--){
            prefij+=prefija.charAt(i);
        }

        return prefij;
    }

    /**
    *  Metodo que me obtiene la expresion Posfija de la notacion 
    * @return expresion convertida en Postfija
    */
    private String getPostfija1() {
        String posfija = "";
        char notacion[] = this.infija.toCharArray();
        Pila<Character> aux = new Pila<>();
        for (int i = 0; i < notacion.length; i++) {
            if (notacion[i] >= '0' && notacion[i] <= '9'||notacion[i]=='~') {
                if(notacion[i]=='~'){
                  posfija+="-";  
                }else{
                posfija += notacion[i];
                }
            } else {
                if (notacion[i] != ')') {
                    if (deboApilar(notacion[i], aux)) {
                        aux.apilar(notacion[i]);
                        if (notacion[i] != '(') {
                            posfija += " ";
                        }

                    } else {
                        char c ;
                        while (!deboApilar(notacion[i], aux)) {
                            c = aux.desapilar();
                            posfija += " " + c;
                        }
                        aux.apilar(notacion[i]);
                        posfija += " ";
                    }
                } else {
                    char c = aux.desapilar();
                    while (c != '(') {
                        posfija += " " + c;
                        c = aux.desapilar();
                    }
                }
            }
        }
        while (!aux.esVacia()) {
            posfija += " " + aux.desapilar();
        }
        return posfija;
    }

    /**
 *  Metodo que me verifica si el tope de la pila es mayor a c
 * @param c caracter sobre el cual voy a realizar la consulta , Pila (p) de signos
 * @return true si el tope es mayor, false si es menor
 */
    private boolean esMayor(char c, Pila<Character> p) {
        if(p.esVacia()){
            return false;
        }
        char tope = p.desapilar();
        p.apilar(tope);
        if (c == '+' || tope == '-') {
            if(tope==')'){
                return false;
            }else  if (tope != '+' && tope != '-') {
                return true;
            }
        } else if (c == '*' || c == '/') {
            return tope == '^';
        }
        return false;
    }
     /**
     * Metodo que me valida si debo apilar o no dependiendo de los signos
     * @param c caracter sobre el cual voy a realizar la consulta , Pila (p) de signos
     * @return true si se apila
     */
    private boolean deboApilar(char c, Pila<Character> p) {
        if (p.esVacia()) {
            return true;
        }
        char tope = p.desapilar();
        p.apilar(tope);//vuelvo a apilar para no afectar el funcionamiento
        switch (c) {
            case '(':
                return true;
            case '+':
            case '-':
        return tope == '(';
            case '*':
            case '/':
        return tope == '(' || tope == '+' || tope == '-';
            case '^':
                return true;
            default:
                break;
        }
        return true;
    }

}
